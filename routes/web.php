<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/paginateste', function(){
    return view('novoteste');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/consulta', 'HomeController@consulta')->name('consulta');
Route::get('/consultar', 'HomeController@consultar')->name('consultar');
Route::get('/alterar', 'HomeController@alterar')->name('alterar');
Route::get('/edicao', 'HomeController@edicao')->name('edicao');
Route::get('/atualiza', 'HomeController@atualiza')->name('atualiza');
Route::get('/cadastro', 'HomeController@cadastro')->name('cadastro');
Route::get('/cadastrar', 'HomeController@cadastrar')->name('cadastrar');
Route::get('/remover', 'HomeController@remover')->name('remover');
Route::get('/excluir', 'HomeController@excluir')->name('excluir');