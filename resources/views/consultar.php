<?php
/**
 * Created by PhpStorm.
 * User: Renato
 * Date: 05/07/2017
 * Time: 14:28
 */
 // Criando tabela e cabeçalho de dados:
 echo "<table border=1>";
 echo "<tr>";
 echo "<th>NOME</th>";
 echo "<th>TIPO</th>";
 echo "<th>PODER ATAQUE</th>";
 echo "<th>PODER DEFESA</th>";
 echo "<th>AGILIDADE</th>";
 echo "</tr>";
 // Conectando ao banco de dados:
 $strcon = mysqli_connect('localhost','root','','pokemon') or die('Erro ao conectar ao banco de dados');
 $sql = "SELECT * FROM pokemon";
 $resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
 // Obtendo os dados por meio de um loop while
 while ($registro = mysqli_fetch_array($resultado))
 {
     $nome = $registro['nome'];
     $tipo = $registro['tipo'];
     $poder_a = $registro['poder_a'];
     $poder_d = $registro['poder_d'];
     $agilidade = $registro['agilidade'];
     echo "<tr>";
     echo "<td>".$nome."</td>";
     echo "<td>".$tipo."</td>";
     echo "<td>".$poder_a."</td>";
     echo "<td>".$poder_d."</td>";
     echo "<td>".$agilidade."</td>";
     echo "</tr>";
 }
 mysqli_close($strcon);
 echo "</table>";
 ?>